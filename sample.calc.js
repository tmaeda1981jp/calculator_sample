/*jslint white: false, nomen: false, */
/*global SAMPLE:false, $:false, */
'use strict';

SAMPLE.util.namespace('SAMPLE.controller');
SAMPLE.controller = (function() {

  var Constr = function(model) {
    this.model = model;
    this.initFlag = true;
  };

  Constr.prototype.initialize = function() {
    var model = this.model,
        $input = $('#user_input'),
        self = this;
    
    $('#plus_button').click(function() {
      model.plus($input.val());
      model.setOperation('+');
      $input.val(model.getTotal());
      self.initFlag = true;
    });
    $('#minus_button').click(function() {
      model.minus($input.val());
      model.setOperation('-');
      $input.val(model.getTotal());
      self.initFlag = true;      
    });
    $('#multiply_button').click(function() {
      model.multiply($input.val());
      model.setOperation('*');
      $input.val(model.getTotal());
      self.initFlag = true;
    });
    $('#divide_button').click(function() {
      model.divide($input.val());
      model.setOperation('/');
      $input.val(model.getTotal());
      self.initFlag = true;
    });
    $('#equal_button').click(function() {
      model.equal($input.val());
      model.setOperation(null);
      $input.val(self.model.getTotal());
      self.initFlag = true;
    });
    $('#clear_button').click(function() {
      self.initFlag = true;
      model.clear();
      $input.val("");
    });
    $('.digit').click(function() {
      if(self.initFlag) {
        $input.val(this.value);
      } else {
        $input.val($input.val() + '' + this.value);
      }
      self.initFlag = false;
    });
  };

  return Constr;
}());


SAMPLE.util.namespace('SAMPLE.model');
SAMPLE.model = (function() {
  var Constr = function() {
    this.total = 0;
    this.operation = null;
  };

  Constr.prototype = {
    plus : function(user_input) {
      if (this.operation === null) {
        this.total = user_input;
        return;
      }
      this.total = parseInt(this.total, 10) + parseInt(user_input, 10);
    },
    minus : function(user_input) {
      if (this.operation === null) {
        this.total = user_input;
        return;
      }
      this.total = parseInt(this.total, 10) - parseInt(user_input, 10);
    },
    multiply : function(user_input) {
      if (this.operation === null) {
        this.total = user_input;
        return;
      }
      this.total = parseInt(this.total, 10) * parseInt(user_input, 10);
    },
    divide : function(user_input) {
      if (this.operation === null) {
        this.total = user_input;
        return;
      }
      this.total = parseInt(this.total, 10) / parseInt(user_input, 10);
    },
    equal : function(user_input) {
      switch(this.operation) {
      case '+' : this.plus(user_input);break;
      case "-" : this.minus(user_input);break;
      case "*" : this.multiply(user_input);break;
      case "/" : this.divide(user_input);break;
      }
    },
    setOperation : function(operation) {
      this.operation = operation;
    },
    clear : function() {
      this.total = 0;
      this.operation = null;
    },
    getTotal : function() {
      return this.total;
    }
  };
  return Constr;
}());

$(function() {
  var model = new SAMPLE.model(),
      controller = new SAMPLE.controller(model);
  controller.initialize();
});