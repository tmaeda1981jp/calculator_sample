/*jslint white: false, nomen: false, */
/*global $:false,*/
'use strict';

var SAMPLE = SAMPLE || {};
SAMPLE.util = (function() {

  /**
   * 名前空間オブジェクトを生成する.
   * @param {String} 名前空間を定義する文字列
   * @return {Object} 名前空間に生成されたオブジェクト
   */
  var namespace = function(str) {
    var parts = str.split('.'),
        parent = SAMPLE,
        i;

    if (parts[0] === 'SAMPLE') {
      parts = parts.slice(1);
    }

    for (i = 0; i < parts.length; i += 1) {
      if (typeof parent[parts[i]] === 'undefined') {
        parent[parts[i]] = {};
      }
      parent = parent[parts[i]];
    }
    return parent;
  };

  return {
    namespace: namespace
  };
}());